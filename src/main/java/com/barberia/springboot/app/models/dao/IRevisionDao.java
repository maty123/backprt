package com.barberia.springboot.app.models.dao;

import com.barberia.springboot.app.models.entity.Revision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IRevisionDao extends JpaRepository<Revision,String> {

    @Query(value = "SELECT * FROM revision WHERE id_revision= ?1", nativeQuery = true)
    Revision forceFindOne(int id_revision);
}
