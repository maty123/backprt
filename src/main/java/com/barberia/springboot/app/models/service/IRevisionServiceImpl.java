package com.barberia.springboot.app.models.service;

import com.barberia.springboot.app.models.dao.IRevisionDao;
import com.barberia.springboot.app.models.entity.Revision;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class IRevisionServiceImpl implements IRevisionService {
   @Autowired
    private IRevisionDao revisionDao;

    @Override
    public List<Revision> findAll() {
        return revisionDao.findAll();
    }

    @Override
    public void save(Revision revision) {
        revisionDao.save(revision);
    }

    @Override
    public Revision findOne(int id_revision) {
        return revisionDao.forceFindOne(id_revision);
    }

    @Override
    public void deleted(int id_revision) {
        Revision r1= revisionDao.forceFindOne(id_revision);
    }
}
