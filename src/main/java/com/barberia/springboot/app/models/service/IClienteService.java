package com.barberia.springboot.app.models.service;

import java.util.List;

import com.barberia.springboot.app.models.entity.Cliente;

public interface IClienteService {

public List<Cliente> findAll();
	
public void save(Cliente cliente);
	
public Cliente findOne(String rut);

public void delete(String rut);


}
