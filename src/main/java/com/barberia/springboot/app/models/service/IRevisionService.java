package com.barberia.springboot.app.models.service;

import com.barberia.springboot.app.models.entity.Revision;

import java.util.List;

public interface IRevisionService {


    List<Revision> findAll();
    void save(Revision revision);
    Revision findOne(int id_revision);
    void deleted (int id_revision);

}
