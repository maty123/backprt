package com.barberia.springboot.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class Vehiculo {

    @Id
    @Column(name = "patente")
    private String patente;

    @NotNull
    @Column(name = "marca")
    private String marca;

    @NotNull
    @Column(name = "modelo")
    private String modelo;

    @NotNull
    @Column(name = "anio")
    private int anio;

    @NotNull
    @Column(name = "tipo_vehiculo")
    private String tipo_vehiculo;

    @NotNull
    @Column(name = "color")
    private String color;

    @NotNull
    @Column(name = "nro_motor")
    private int nro_motor;

    @NotNull
    @Column(name = "nro_chasis")
    private int nro_chasis;

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getTipo_vehiculo() {
        return tipo_vehiculo;
    }

    public void setTipo_vehiculo(String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNro_motor() {
        return nro_motor;
    }

    public void setNro_motor(int nro_motor) {
        this.nro_motor = nro_motor;
    }

    public int getNro_chasis() {
        return nro_chasis;
    }

    public void setNro_chasis(int nro_chasis) {
        this.nro_chasis = nro_chasis;
    }
}
