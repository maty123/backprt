package com.barberia.springboot.app.models.dao;

import com.barberia.springboot.app.models.entity.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IVehiculoDao extends JpaRepository<Vehiculo,String> {
    @Query(value = "SELECT * FROM vehiculo WHERE patente= ?1", nativeQuery = true)
    Vehiculo forceFindOne(String patente);
}
