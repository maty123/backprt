package com.barberia.springboot.app.models.service;

import com.barberia.springboot.app.models.dao.IVehiculoDao;
import com.barberia.springboot.app.models.entity.Vehiculo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class VehiculoServiceImpl implements IVehiculoService {
    @Autowired
    private IVehiculoDao vehiculoDao;
    @Override
    public List<Vehiculo> findAll() {
        return vehiculoDao.findAll();
    }

    @Override
    public void save(Vehiculo vehiculo) {
        vehiculoDao.save(vehiculo);
    }

    @Override
    public Vehiculo findOne(String patente) {
        return vehiculoDao.forceFindOne(patente);
    }

    @Override
    public void deleted(String patente) {
        Vehiculo v1=findOne(patente);
        vehiculoDao.delete(v1);
    }
}
