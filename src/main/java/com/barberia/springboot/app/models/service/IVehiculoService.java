package com.barberia.springboot.app.models.service;

import com.barberia.springboot.app.models.entity.Vehiculo;

import java.util.List;

public interface IVehiculoService {
    List<Vehiculo> findAll();
    void save(Vehiculo vehiculo);
    Vehiculo findOne(String patente);
    void deleted (String patente);

}
