package com.barberia.springboot.app.controllers;

import com.barberia.springboot.app.models.entity.Cliente;
import com.barberia.springboot.app.models.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.List;

@Controller
@SessionAttributes("cliente")
public class clienteController {
    @Autowired
    private IClienteService clienteService;

    @GetMapping(value = "/listar")
    public List<Cliente> listar(){
        List<Cliente> todaRevision = clienteService.findAll();
        return  todaRevision;
    }

    @PostMapping(value = "/crear")
    public ResponseEntity<Cliente> create(@RequestBody Cliente cliente,
                                           BindingResult bindingResult) {
        if (cliente.getRut() == null && cliente.getNombre() == null) {
            return new ResponseEntity<Cliente>(HttpStatus.NOT_ACCEPTABLE);
        }
        clienteService.save(cliente);
        return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
    }

    @PutMapping(value ="/cliente/update/{rut}")
    public ResponseEntity<Cliente> update(@RequestBody Cliente cliente,
                                           @PathVariable String rut) {
        Cliente clienteActual = clienteService.findOne(rut);
        if (clienteActual == null) {
            return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
        }
        clienteActual.setNombre(cliente.getNombre());
        clienteActual.setCorreo(cliente.getCorreo());
        clienteActual.setTelefono(cliente.getTelefono());
        clienteService.save(clienteActual);

        return new ResponseEntity<Cliente>(clienteActual, HttpStatus.ACCEPTED);
    }
    //delete
    @DeleteMapping(value = "cliente/delete/{rut}")
    public ResponseEntity<Void> delete(@PathVariable String rut) {
        Cliente clienteActual = clienteService.findOne(rut);
        if (clienteActual == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        clienteService.delete(rut);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }
}
