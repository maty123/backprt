package com.barberia.springboot.app.controllers;


import com.barberia.springboot.app.models.entity.Vehiculo;
import com.barberia.springboot.app.models.service.IVehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@SessionAttributes("vehiculo")
public class vehiculoController {

    @Autowired
    private IVehiculoService vehiculoService;

    @GetMapping(value = "/listar")
    public List<Vehiculo> listar(){
        List<Vehiculo> todaRevision =vehiculoService.findAll();
        return  todaRevision;
    }

    @PostMapping(value = "/crear")
    public ResponseEntity<Vehiculo> create(@RequestBody Vehiculo vehiculo,
                                           BindingResult bindingResult) {
        if (vehiculo == null) {
            return new ResponseEntity<Vehiculo>(HttpStatus.NOT_ACCEPTABLE);
        }
        vehiculoService.save(vehiculo);
        return new ResponseEntity<Vehiculo>(vehiculo, HttpStatus.CREATED);
    }

    @PutMapping(value ="/vehiculo/update/{patente}")
    public ResponseEntity<Vehiculo> update(@RequestBody Vehiculo vehiculo,
                                           @PathVariable String patente) {
        Vehiculo vehiculoActual = vehiculoService.findOne(patente);
        if (vehiculoActual == null) {
            return new ResponseEntity<Vehiculo>(HttpStatus.NOT_FOUND);
        }
        vehiculoActual.setColor(vehiculo.getColor());

        return new ResponseEntity<Vehiculo>(vehiculoActual, HttpStatus.ACCEPTED);
    }
    //delete
    @DeleteMapping(value = "vehiculo/delete/{patente}")
    public ResponseEntity<Void> delete(@PathVariable String patente) {
        Vehiculo vehiculoActual = vehiculoService.findOne(patente);
        if (vehiculoActual == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        vehiculoService.deleted(patente);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }
}
