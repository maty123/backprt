package com.barberia.springboot.app.controllers;


import com.barberia.springboot.app.models.entity.Revision;
import com.barberia.springboot.app.models.service.IRevisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@SessionAttributes("revision")
public class revisionController {

    @Autowired
    private IRevisionService revisionService;

    @GetMapping(value = "/listar")
    public List<Revision> listar(){
        List<Revision> todaRevision =revisionService.findAll();
        return  todaRevision;
    }

    @PostMapping(value = "/crear")
    public  ResponseEntity<Revision> create(  @RequestBody Revision revision,
                                              BindingResult bindingResult) {
        if (revision.getFecha() == null) {
            return new ResponseEntity<Revision>(HttpStatus.NOT_ACCEPTABLE);
        }
        revisionService.save(revision);
        return new ResponseEntity<Revision>(revision, HttpStatus.CREATED);
    }

    @PutMapping(value ="/revision/update/{id}")
    public ResponseEntity<Revision> update(@RequestBody Revision revision,
                                           @PathVariable int id) {
        Revision revisionActual = revisionService.findOne(id);
        if (revisionActual == null) {
            return new ResponseEntity<Revision>(HttpStatus.NOT_FOUND);
        }
        revisionActual.setFecha(revision.getFecha());
        revisionActual.setVehiculo(revision.getVehiculo());
        revisionService.save(revisionActual);
        return new ResponseEntity<Revision>(revisionActual, HttpStatus.ACCEPTED);
    }
//delete
    @DeleteMapping(value = "revision/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable int id) {
        Revision revisionActual = revisionService.findOne(id);
        if (revisionActual == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        revisionService.deleted(id);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }


}
